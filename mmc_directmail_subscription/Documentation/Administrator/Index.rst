﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================


.. _admin-installation:

Installation
------------


To install the extension, perform the following steps:

#. Go to the Extension Manager
#. Install the extension
#. Load the static template


The extensions requires 'direct_mail' and 'tt_address' to be installed!



