﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

This is a simple replacement for the direct_mail_subscription extension.
It's buit on extbase, so translations and templating is much easier. To prevent from spam,
'honeypot' instead of captcha is used.

It allows FE-users to

- Subscript to the newsletter (user receives a mail to confirm his email-address)
- Cancel newsletter subscription (user receives a mail to confirm his email-address)

Supported tt_address fields are

- gender
- firstName
- lastName
- name (will be set to 'firstName lastName' by default)
- moduleSysDmailHtml (by default set to 1 by a hidden field in the registration form)

By default, on subscription all existing records with identical email-adresses will be
removed. This option can be disabled by TS-constant.

