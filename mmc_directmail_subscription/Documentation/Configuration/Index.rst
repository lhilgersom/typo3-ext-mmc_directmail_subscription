﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

Configuration Reference
=======================


.. _configuration-typoscript:



Typoscript Constants:
^^^^^^^^^^^^^^^^^^^^^^
Keep email-address unique (default: 1)
	remove tt_address records with the same email-address on subscription

Include jQuery library
	disable if jQuery is already available (extension uses jQuery to validate form fields)

Email sender address
	Email sender address for address-verification-emails. If not set, no mails will be sent!

Email sender name
	Email sender name for address-verification-emails


Storage for tt_address
^^^^^^^^^^^^^^^^^^^^^^
- 'Default storage PID' of the extension
- 'Record Storage Page' of the FE-Plugin
