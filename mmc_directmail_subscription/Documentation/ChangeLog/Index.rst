﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _changelog:

ChangeLog
=========

0.9.5
-----
- email repeat check in forms

0.9.6
------
- added FE-language labels for Dutch (nl). Thanks to Stefan Rosbak (srosbak@zigwebsoftware.nl)
- simplified honeypot implementation
If you copied and customized the template, you have to consider change for the hidden honeypot fields!