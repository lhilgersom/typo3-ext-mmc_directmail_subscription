<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'MMC.' . $_EXTKEY,
	'Subscr',
	array(
		'Subscribe' => 'register, registerConfirm, cancel, cancelConfirm',
		
	),
	// non-cacheable actions
	array(
		'Subscribe' => 'register, registerConfirm, cancel, cancelConfirm',
		
	)
);
