<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Subscr',
	'MMC directmail subscription'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MMC directmail subscription');


/* extend TCA for tt_address */

$tempColumns = Array (
	"email_verification_code" => Array (           
  	"config" => Array ( "type" => "passthrough" )
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("tt_address", $tempColumns);