
plugin.tx_mmcdirectmailsubscription {
	view {
		templateRootPath = {$plugin.tx_mmcdirectmailsubscription.view.templateRootPath}
		partialRootPath = {$plugin.tx_mmcdirectmailsubscription.view.partialRootPath}
		layoutRootPath = {$plugin.tx_mmcdirectmailsubscription.view.layoutRootPath}
	}
	settings{
		scriptPath = {$plugin.tx_mmcdirectmailsubscription.settings.scriptPath}	
		keepEmailAddressUnique = {$plugin.tx_mmcdirectmailsubscription.settings.keepEmailAddressUnique}	
		fromEmail = {$plugin.tx_mmcdirectmailsubscription.settings.fromEmail}	
		fromName = {$plugin.tx_mmcdirectmailsubscription.settings.fromName}	
	}
	persistence {
		storagePid = {$plugin.tx_mmcdirectmailsubscription.persistence.storagePid}
		classes{
			MMC\MmcDirectmailSubscription\Domain\Model\Address {
				mapping{
					tableName = tt_address
				}
			}
		}
	}
	
}

[globalVar = LIT:1 = {$plugin.tx_mmcdirectmailsubscription.settings.includeJQuery} ]
	page.includeJS.tx_mmcdirectmailsubscription_jquery = {$plugin.tx_mmcdirectmailsubscription.settings.scriptPath}jquery-1.8.3.min.js
[global]

plugin.tx_mmcdirectmailsubscription._CSS_DEFAULT_STYLE (
  .tx-mmc-directmail-subscription .form-row label{ display:block }
  .tx-mmc-directmail-subscription .form-row.error label{ color:#D00 }
  .tx-mmc-directmail-subscription .form-row.error input, 
  .tx-mmc-directmail-subscription .form-row.error textarea{ 
  	color:#D00;  
  	border-bottom: 1px dotted #D00;
  }
	.tx-mmc-directmail-subscription input.h { display:none }
)
